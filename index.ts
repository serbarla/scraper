import puppeteer from "puppeteer";
import { Browser } from "puppeteer";
import { promises as fsPromises } from "fs";
import { join } from "path";

async function asyncWriteFile(filename: string, data: any) {
  try {
    await fsPromises.writeFile(join(__dirname, filename), data, {
      flag: "w",
    });

    const contents = await fsPromises.readFile(
      join(__dirname, filename),
      "utf-8"
    );

    return contents;
  } catch (err) {
    console.log(err);
    return "Something went wrong";
  }
}

const ligapro2021 = async (url: string, selector: string) => {
  const brwsr: Browser = await puppeteer.launch({ headless: "new" });
  const page = await brwsr.newPage();
  await page.goto(url);

  const data = await page.evaluate((selector) => {
    const prepass = Array.from(document.querySelectorAll(selector));
    const interno = prepass.map((tablas) => {
      return Array.from(tablas.querySelectorAll("tr"))
        .map((partido, index1) => {
          if (index1 > 1) {
            return Array.from(partido.querySelectorAll("td"))
              .map((x, xindex) => x.textContent)
              .filter((f, index) => index === 0 || index === 2 || index === 4)
              .join(",")
              .trim();
          } else {
            return "NA";
          }
        })
        .filter((c, ind) => ind > 1);
    });
    return interno;
  }, selector);
  console.log(data);
  //asyncWriteFile("./data23.txt", data);

  await brwsr.close();
};

const main = async () => {
/*   const url2021 = "https://es.wikipedia.org/wiki/Serie_A_de_Ecuador_2021";
  const selector2021 =
    "#mw-content-text > div.mw-parser-output > center:nth-child(55) > table"; */
/*   const url2022 = "https://es.wikipedia.org/wiki/Serie_A_de_Ecuador_2022";
  const selector2022 =
    "#mw-content-text > div.mw-parser-output > table"; */
  const url2023 = "https://es.wikipedia.org/wiki/Serie_A_de_Ecuador_2023";
  const selector2023 =
    "#mw-content-text > div.mw-parser-output > table";
  await ligapro2021(url2023, selector2023);
};

main();
